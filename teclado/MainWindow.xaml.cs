﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace teclado
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CargarTeclado()
        {
            DockPanel dp = new DockPanel();
            

            Button b = new Button();
            b.Content ="Aceptar";
            b.Height = 30;
            b.VerticalAlignment = VerticalAlignment.Top;
            b.HorizontalAlignment = HorizontalAlignment.Right;

            dp.Children.Add(new Numeros());
            dp.Children.Add(b);
           
            gdDinamico.Children.Add(dp);
        }

        private void CargarControles()
        {
            RowDefinition rd1 = new RowDefinition();
            RowDefinition rd2 = new RowDefinition();
            gdDinamico.RowDefinitions.Add(rd1);
            gdDinamico.RowDefinitions.Add(rd2);


            Grid header = CargarHeader();
            Grid footer = CargarDigitos();
            Grid.SetRow(header, 0);
            Grid.SetRow(footer, 1);
            
            gdDinamico.Children.Add(header);
            gdDinamico.Children.Add(footer);
        }

        private  Grid CargarHeader()
        {
            Grid grid = new Grid();
            grid.Width = 200;
            grid.Height = 250;
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            grid.VerticalAlignment = VerticalAlignment.Top;
            //g.ShowGridLines = true;

            // Define the Columns
            ColumnDefinition cd1 = new ColumnDefinition();
            cd1.Width = GridLength.Auto;
            ColumnDefinition cd2 = new ColumnDefinition();
            cd2.Width = GridLength.Auto;
            ColumnDefinition cd3 = new ColumnDefinition();
            grid.ColumnDefinitions.Add(cd1);
            grid.ColumnDefinitions.Add(cd2);
            grid.ColumnDefinitions.Add(cd3);

            Label l = new Label();
            l.Content = "Cantidad";
            
            TextBox t = new TextBox();
            t.Height = 30;

            Button b = new Button();
            b.Content = "Aceptar";

            Grid.SetColumn(l, 0);
            Grid.SetColumn(t, 1);
            Grid.SetColumn(b, 2);
            grid.Children.Add(l);
            grid.Children.Add(t);
            grid.Children.Add(b);
            return grid;
        }

        private  Grid CargarDigitos()
        {
            Grid grid = new Grid();
            grid.Width = 200;
            grid.Height = 250;
            grid.HorizontalAlignment = HorizontalAlignment.Left;
            grid.VerticalAlignment = VerticalAlignment.Top;
            //g.ShowGridLines = true;

            // Define the Columns
            ColumnDefinition cd1 = new ColumnDefinition();
            ColumnDefinition cd2 = new ColumnDefinition();
            ColumnDefinition cd3 = new ColumnDefinition();
            grid.ColumnDefinitions.Add(cd1);
            grid.ColumnDefinitions.Add(cd2);
            grid.ColumnDefinitions.Add(cd3);

            // Define the Rows
            RowDefinition rd1 = new RowDefinition();
            RowDefinition rd2 = new RowDefinition();
            RowDefinition rd3 = new RowDefinition();
            RowDefinition rd4 = new RowDefinition();
            grid.RowDefinitions.Add(rd1);
            grid.RowDefinitions.Add(rd2);
            grid.RowDefinitions.Add(rd3);
            grid.RowDefinitions.Add(rd4);

            Button b9 = new Button();
            b9.Content = "9";
            b9.FontWeight = FontWeights.Bold;

            Button b8 = new Button();
            b8.Content = "8";
            b8.FontWeight = FontWeights.Bold;

            Button b7 = new Button();
            b7.Content = "7";
            b7.FontWeight = FontWeights.Bold;

            Button b6 = new Button();
            b6.Content = "6";
            b6.FontWeight = FontWeights.Bold;

            Button b5 = new Button();
            b5.Content = "5";
            b5.FontWeight = FontWeights.Bold;

            Button b4 = new Button();
            b4.Content = "4";
            b4.FontWeight = FontWeights.Bold;

            Button b3 = new Button();
            b3.Content = "3";
            b3.FontWeight = FontWeights.Bold;

            Button b2 = new Button();
            b2.Content = "2";
            b2.FontWeight = FontWeights.Bold;

            Button b1 = new Button();
            b1.Content = "1";
            b1.FontWeight = FontWeights.Bold;

            Button b0 = new Button();
            b0.Content = "0";
            b0.FontWeight = FontWeights.Bold;

            Button bDel= new Button();
            bDel.Content = "Del";
            bDel.FontWeight = FontWeights.Bold;

            Grid.SetColumn(b9, 2);
            Grid.SetRow(b9, 0);

            Grid.SetColumn(b8, 1);
            Grid.SetRow(b8, 0);

            Grid.SetColumn(b7, 0);
            Grid.SetRow(b7, 0);

            Grid.SetColumn(b6, 2);
            Grid.SetRow(b6, 1);

            Grid.SetColumn(b5, 1);
            Grid.SetRow(b5, 1);

            Grid.SetColumn(b4, 0);
            Grid.SetRow(b4, 1);

            Grid.SetColumn(b3, 2);
            Grid.SetRow(b3, 2);

            Grid.SetColumn(b2, 1);
            Grid.SetRow(b2, 2);

            Grid.SetColumn(b1, 0);
            Grid.SetRow(b1, 2);

            Grid.SetColumnSpan(b0, 1);
            Grid.SetRow(b0, 3);

            Grid.SetColumn(bDel, 2);
            Grid.SetRow(bDel, 3);

            grid.Children.Add(b9);
            grid.Children.Add(b8);
            grid.Children.Add(b7);
            grid.Children.Add(b6);
            grid.Children.Add(b5);
            grid.Children.Add(b4);
            grid.Children.Add(b3);
            grid.Children.Add(b2);
            grid.Children.Add(b1);
            grid.Children.Add(b0);
            grid.Children.Add(bDel);

            return grid;
        }

        private void botonDigitos_Click(object sender, RoutedEventArgs e)
        {
            switch (((Button)sender).Content.ToString())
            {
                case "Del":
                    if (txtCantidad.Text.Length>0)
                    {
                        txtCantidad.Text = txtCantidad.Text.Remove(txtCantidad.Text.Length - 1);
                    }
                    else
                    {
                        txtCantidad.Text = String.Empty;
                    }
                    
                    break;
                default:
                    if (txtCantidad.Text.Length ==0)
                    {
                        if (((Button)sender).Content.ToString() != "0")
                        {
                            txtCantidad.Text += ((Button)sender).Content.ToString();
                        }
                    }
                    else
                    {
                        txtCantidad.Text += ((Button)sender).Content.ToString();
                    }
                    break;
            }
        }
    }
}
